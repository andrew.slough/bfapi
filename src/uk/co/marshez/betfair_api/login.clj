(ns uk.co.marshez.betfair-api.login
  (:require [http.async.client :as http]
            [http.async.client.cert :as cert]
            [clojure.data.json :as json]
            [clojure.spec.alpha :as s]
            [clojure.walk :refer [keywordize-keys]]
            [clojure.core.async :as async :refer [go chan >! <!! >!! close! thread]]))

(s/def ::loginStatus #{"SUCCESS"})
(s/def ::sessionToken string?)
(s/def ::login-response (s/keys :req-un [::sessionToken ::loginStatus]))

(defn login
  "Synchronously Login to betfair using the login endpoint, username,
  password and client-side certificate specified in credentials. Http
  errors, non-json bodies and non-conforming results return nil."
  ([{:keys [ks-file cert-file cert-alias ks-passwd login-endpoint application-key username password] :as creds} response-ch]
   (thread
     (try
       (let [ctx (cert/ssl-context :keystore-file ks-file
                                   :certificate-file cert-file
                                   :certificate-alias cert-alias
                                   :keystore-password ks-passwd)]
         (with-open [client (http/create-client :ssl-context ctx)]
           (let [query    {:username username :password password}
                 headers  {:X-Application application-key
                           :accept        "application/json"}
                 response (http/await (http/POST client login-endpoint :query query :headers headers))]
             (assert (= 200 (:code @(:status response))))
             (let [data (-> @(:body response) .toByteArray String. json/read-str keywordize-keys (merge creds))]
               (if (s/valid? ::login-response data)
                 (>!! response-ch data)
                 (throw (Exception. (s/explain-str ::login-response data))))))))
       (catch Exception e (>!! response-ch e))))
   response-ch)
  ([creds] (login creds (chan))))

;; (spec/fdef login
;;            :args (spec/cat :credentials ::credentials)
;;            :ret (spec/or :success ::login-response :error nil?))
