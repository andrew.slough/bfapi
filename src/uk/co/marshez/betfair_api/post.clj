(ns uk.co.marshez.betfair-api.post
  (:require [clj-http.client :as http]
            [clojure.data.json :as json]
            [taoensso.timbre :as timbre :refer [log  trace  debug  info  warn  error  fatal  report]]
            [clojure.string :refer [join]]
            [clojure.core.async :as async :refer [>!!]]))

(defn ^:private post-endpoint
  [endpoint credentials request result-chan]
  (http/post endpoint {:headers {:X-Application (:application-key credentials)
                                 :X-Authentication (:sessionToken credentials)
                                 :Content-Type "application/json"
                                 :accept "application/json"}
                       :body (json/write-str request)
                       :async? true}
             (fn [response] (>!! result-chan response))
             (fn [exception] (error "Exception making post to " endpoint " request " (str request) " exception " (.getMessage exception)))))

(defmulti post (fn [api-type _ _ _ _] api-type))
(defmethod post :betting
  [_ endpoint-part credentials request result-chan]
  (post-endpoint (join [(:betting-endpoint credentials) endpoint-part "/"]) credentials request result-chan))
(defmethod post :account
  [_ endpoint-part credentials request result-chan]
  (post-endpoint (join [(:account-endpoint credentials) endpoint-part "/"]) credentials request result-chan))
