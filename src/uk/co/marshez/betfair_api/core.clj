(ns uk.co.marshez.betfair-api.core
  (:require [http.async.client.cert :as cert]
            [http.async.client :as http]
            [clojure.walk :refer [keywordize-keys]]
            [clojure.data.json :as json]
            [clojure.spec.alpha :as spec]
            [clojure.core.async :as async :refer [chan <!!]]
            [uk.co.marshez.betfair-api.post :refer [post]]
            [uk.co.marshez.betfair-api.login :refer [login]]
            [taoensso.timbre :as timbre :refer [log  trace  debug  info  warn  error  fatal  report]]))

(spec/def ::username string?)
(spec/def ::password string?)
(spec/def ::application-key string?)
(spec/def ::login-endpoint string?)
(spec/def ::ks-file string?)
(spec/def ::ks-passwd string?)
(spec/def ::cert-file string?)
(spec/def ::cert-alias string?)
(spec/def ::credentials (spec/keys :req-un [::username ::password ::application-key ::login-endpoint
                                            ::ks-file ::ks-passwd ::cert-file ::cert-alias]))

(defn account-funds [credentials]
  (let [c (chan)]
    (post :account "getAccountFunds" credentials {} c)
    (<!! c)))

(defn -main [])
